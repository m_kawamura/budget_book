100.times do |n|
  month    = ('1'..'12').to_a.shuffle[0]
  day      = ('1'..'31').to_a.shuffle[0]
  category = ["朝食", "昼食", "夕食",
              "趣味", "食材", "その他"].shuffle[0]
  amount   = ('500'..'2000').to_a.shuffle[0]
  Spending.create!(month:    month,
                   day:      day,
                   category: category,
                   amount:   amount)
  end