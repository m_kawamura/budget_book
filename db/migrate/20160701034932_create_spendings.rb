class CreateSpendings < ActiveRecord::Migration
  def change
    create_table :spendings do |t|
      t.integer :month
      t.integer :day
      t.string  :category
      t.integer :amount

      t.timestamps null: false
    end
  end
end
