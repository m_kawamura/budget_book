class Spending < ActiveRecord::Base
  
  # 「カテゴリ」のバリデーション
  validates :category,  presence: true
  
  # 「金額」のバリデーション
  VALID_AMOUNT_REGEX = /\A[1-9]+[0-9]*\z/
  validates :amount,    presence: true, length: { maximum: 6 },
                        format: { with: VALID_AMOUNT_REGEX }
end
