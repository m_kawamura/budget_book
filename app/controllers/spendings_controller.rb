class SpendingsController < ApplicationController
  before_action :detect_devise_variant
  
  # 支出記録を表示
  def index
    if (params[:switch].to_i == 0) 
      @day = Time.now.month      # 最初に表示する月を、ウィンドウから取得
    else
      @day = params[:switch].to_i   # 次に表示する月を、パラメータから取得
    end
    @spendings = Spending.where(month: @day).order('day DESC')    # 次に表示する月のレコードを降順で取得
    
    @spending = Spending.new
  end
  
  # 支出記録を保存
  def create
    @spending = Spending.new(spending_params)
    if @spending.save
      redirect_to :back
    else
      redirect_to :back, :alert => '"金額"が無効な値です'
    end
  end
  
  # 支出記録を削除
  def destroy
    Spending.find(params[:id]).destroy
    redirect_to :back
  end
  
  private
    
    # StrongParameter
    def spending_params
      params.require(:spending).permit(:month, :day, :category, :amount)
    end
    
    # ビューのモバイル対応
    def detect_devise_variant
      case request.user_agent
      when /iPhone/
        request.variant = :iphone
      when /Android/
        request.variant = :android
      end
    end
    
end
