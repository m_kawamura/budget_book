Rails.application.routes.draw do
  
  # メインページへのルート
  root              'spendings#index'
  resources :spendings, only: [:index, :new, :create, :destroy]
  
end
